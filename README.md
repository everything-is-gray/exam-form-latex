# exam-form-latex

This was meant to be a customizable form for medical play and scene negotiation written in LaTex.  Each section can be included or removed to create a guide that can be used in scenes.

A generated pdf is included with each release.  Or you can generate your own from the source using LaTex.

This project has been abandoned to work on a new responsive and fillable form written in Vue.  The new project can be found at <https://codeberg.org/everything-is-gray/exam-form-vue>
